package com.example.application;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Magnifier;
import android.widget.TextView;

public class Biometric extends AppCompatActivity {

    private TextView Heading, Label;
    private ImageView FingerprintImage;
    FingerprintManager fingerprintManager;
    KeyguardManager keyguardManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_biometric);

        Heading = findViewById(R.id.Heading);
        FingerprintImage = findViewById(R.id.Fingure);
        Label = findViewById(R.id.Label);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

            if(!fingerprintManager.isHardwareDetected()){

                Label.setText("Error");

            }else if(ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){

                Label.setText("not granted to use");
            }else if(!keyguardManager.isKeyguardSecure()){

                Label.setText("Add something is setting");

            }else if(!fingerprintManager.hasEnrolledFingerprints()){
                Label.setText("Should have i fingerprint to use");
            }else{
                Label.setText("Detect your Fingerprint to login");

                FingerprintHandler fingerprintHandler = new FingerprintHandler(this);
                fingerprintHandler.startAuth(fingerprintManager, null);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

        Context context;

        public FingerprintHandler(Context context){

            this.context = context;

        }

        public void startAuth(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){

            CancellationSignal cancellationSignal = new CancellationSignal();

            fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0, this, null);

        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {

            this.update("There was an Auth Error." + errString, false);
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

            this.update("Error: "+ helpString, false);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

            this.update("access the application", true);

        }

        private void update(String s, boolean b) {

            TextView Label = ((Activity)context).findViewById(R.id.Label);
            ImageView Finge = ((Activity)context).findViewById(R.id.Fingure);

            Label.setText(s);

            if(b == false){

                Label.setTextColor(ContextCompat.getColor(context, R.color.red));


            }else{

                Label.setTextColor(ContextCompat.getColor(context, R.color.title));
                Finge.setImageResource(R.mipmap.ic_done);
                Intent home = new Intent(getApplicationContext(), Home.class);
                startActivity(home);
            }
        }
    }
}
