package com.example.application;

public class Products {

    private String uDescr;
    private String uImage;
    private String uTitle;
    private String uTime;

    public Products() {
    }

    public Products(String uDescr, String uImage, String uTitle, String uTime) {
        this.uDescr = uDescr;
        this.uImage = uImage;
        this.uTitle = uTitle;
        this.uTime = uTime;
    }

    public String getuDescr() {
        return uDescr;
    }

    public void setuDescr(String uDescr) {
        this.uDescr = uDescr;
    }

    public String getuImage() {
        return uImage;
    }

    public void setuImage(String uImage) {
        this.uImage = uImage;
    }

    public String getuTitle() {
        return uTitle;
    }

    public void setuTitle(String uTitle) {
        this.uTitle = uTitle;
    }

    public String getuTime() {
        return uTime;
    }

    public void setuTime(String uTime) {
        this.uTime = uTime;
    }

}
