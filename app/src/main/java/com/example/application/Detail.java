package com.example.application;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Detail extends AppCompatActivity {

    String Title, Description, Image;
    ImageView image, back;
    TextView title, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        image = findViewById(R.id.image);
        back = findViewById(R.id.back);

        getData();
        setData();

        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Home.class));
            }
        });
    }


    private void getData(){
        if(getIntent().hasExtra("data1") && getIntent().hasExtra("data2") && getIntent().hasExtra("data3")){
            Title = getIntent().getStringExtra("data1");
            Description = getIntent().getStringExtra("data2");
            Image = getIntent().getStringExtra("data3");
        }else {

        }
    }
    private void setData(){
        title.setText(Title);
        description.setText(Description);
        Picasso.get().load(Image).into(image);

    }

}
