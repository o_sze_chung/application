package com.example.application;

import android.widget.EditText;

import androidx.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.junit.Assert.*;


public class RegisterTest {

    @Rule
    public ActivityTestRule<Register> activityTestRule = new ActivityTestRule<>(Register.class);

    @Test
    public void register_Acctivity() {
        onView(withId(R.id.username)).perform(typeText("1234567@gmail.com"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.registerbtn)).perform(click());
    }
}